### Project Description ###
This project contains a program written in C that displays a textured terrain using a polygonal mesh that is generated based on height values.  
The height values are transformed to relative values to fit a resonable size on the scene (relative = height/max_height * 6),  
where 6 represents the maximum height in this case.  
  
The scene contains also a rotating sun (a sphere with a shiny yellow material) that simulates sunrise and sunset.  
  
In addition, the scene has falling spheres with different sizes and speeds. The falling spheres' color  
and falling speed can be changed interactively using a menu that is accessible by right clicking on the mouse.  

Another interactive feature available in the program is the ability to hit a falling sphere by clicking with the left  
mouse button. Doing so displays a message in the command prompt saying that a sphere was hit.  
  
The user can fly over the scene using the following keys:  
l: move right  
j: move left  
i: move forward  
k: move backward  
  
u: move up  
n: move down  

The scene can be rotated as well using the arrow keys.  

### Libraries Used ###
* GLEW 1.13.0
* FREEGLUT 3.0.0-1
