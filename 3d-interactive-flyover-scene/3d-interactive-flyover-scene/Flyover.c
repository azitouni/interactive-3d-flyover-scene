/* This program displays a textured terrain using a polygonal mesh that is generated based on height values.
The height values are transformed to relative values to fit a resonable size on the scene (relative = height/max_height * 6),
where 6 represents the maximum height in this case.

The scene contains also a rotating sun (a sphere with a shiny yellow material) that simulates sunrise and sunset.

In addition, the scene has falling spheres with different sizes and speeds. The falling spheres' color
and falling speed can be changed interactively using a menu that is accessible by right clicking on the mouse.

Another interactive feature available in the program is the ability to hit a falling sphere by clicking with the left
mouse button. Doing so displays a message in the command prompt saying that a sphere was hit.

The user can fly over the scene using the following keys:
l: move right
j: move left
i: move forward
k: move backward

u: move up
n: move down

The scene can be rotated as well using the arrow keys.

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL\freeglut.h>

// matrix that contains the vertices of the mesh
GLfloat vertices2[436][465];
GLuint textures[3];

static GLfloat theta[] = { 0.0, 0.0, 0.0 };
static GLuint axis = 2;
// initial viewer location
static GLdouble viewer[] = { 10, 5, 40, 0.0, 0.0, -1 };
GLubyte image[256][256][3];

int m, n, i, j, M = 23, N = 19;


// variables for storing random decimal values in the range [0,1)
double r;
double r2;
// falling position of the spheres
float falling = 25;
// speed of the falling spheres
float speed = 0.01;

float angle = 0.0, sun_pos = 0.0;

#define SIZE 512
#define MAX_RELATIVE_SIZE 6

// initial position of the sun
float sun_position[3] = { 15, 0, -5 };

int sphere_index = 99;

float falling_speed[10];

typedef struct f_sphere{
	float size;
	float f_speed;
	GLUquadricObj *sphere;

}f_sphere;

// falling spheres array
f_sphere FS[5];


typedef struct materialStruct{
	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
	GLfloat shininess;
}materialStruct;

// material properties of the sun
materialStruct whiteShinyMaterials = {
	{ 1.0, 1.0, 1.0, 1.0 },
	{ 1.0, 1.0, 0.0, 1.0 },
	{ 1.0, 1.0, 1.0, 1.0 },
	100
};
// material properties of a falling sphere (shiny blue and shiny green)
materialStruct blueShinyMaterials = {
	{ 0.5, 1.0, 0.2, 1.0 },
	{ 0.0, 0.1, 1.0, 1.0 },
	{ 1.0, 1.0, 1.0, 1.0 },
	100
};
materialStruct GreenShinyMaterials = {
	{ 0.5, 1.0, 0.2, 1.0 },
	{ 0.0, 1.0, 0.0, 1.0 },
	{ 1.0, 1.0, 1.0, 1.0 },
	100
};

materialStruct *currentMaterials;
materialStruct *currentMaterials2 = &blueShinyMaterials;

void spheres_rand_speed()
{
	srand(20);


	for (i = 0; i < 5; i++){

		double randomValue = rand();
		FS[i].f_speed = speed + (sin(randomValue) / 100); // just a simple calculation for a random speed
	}
}

// Function that applies a material property on a falling sphere
void s_material(materialStruct *currentMaterials2)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glMaterialfv(GL_FRONT, GL_AMBIENT, currentMaterials2->ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, currentMaterials2->diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, currentMaterials2->specular);
	glMaterialf(GL_FRONT, GL_SHININESS, currentMaterials2->shininess);

}

void draw_spheres(GLenum mode)
{
	srand(20);
	spheres_rand_speed();
	for (i = 0; i < 5; i++)
	{
		r = ((double)rand() / ((double)(RAND_MAX)+(double)(1)));

		glPushMatrix();
		glPushAttrib(GL_LIGHTING_BIT);
		s_material(currentMaterials2);
		FS[i].sphere = gluNewQuadric();
		gluQuadricDrawStyle(FS[i].sphere, GL_FILL);

		glTranslatef(rand() % 7 - (i * 5), falling_speed[i], 20); // random position of the falling spheres
		if (mode == GL_SELECT) {
			glLoadName(i); // name the spheres for picking
		}

		gluQuadricNormals(FS[i].sphere, GL_SMOOTH);
		gluSphere(FS[i].sphere, (r*1.5), 60, 60); // drawing the sphere with a random size

		glPopAttrib();
		glPopMatrix();

	}
	glFlush();
}

void sun_material()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	currentMaterials = &whiteShinyMaterials;
	glMaterialfv(GL_FRONT, GL_AMBIENT, currentMaterials->ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, currentMaterials->diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, currentMaterials->specular);
	glMaterialf(GL_FRONT, GL_SHININESS, currentMaterials->shininess);

}

void draw_Sun()
{

	// Translating the sun to the initial position
	glTranslatef(sun_position[0], sun_position[1], sun_position[2]);


	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glPushAttrib(GL_LIGHTING_BIT);
	sun_material();

	// rotating the sun
	glTranslatef(cos(sun_pos) * 10, -sin(sun_pos) * 10, sun_position[2]);

	glutSolidSphere(1.5, 60, 60);

	glPopAttrib();
	glPopMatrix();
	glFlush();
}

// Function that loads the land heights from a file
void LoadHeights(char *filePath)
{
	float height_data;
	FILE *file;

	errno_t err;
	err = fopen_s(&file, filePath, "rb");
	if (err != 0)
	{
		printf("Could not open the file %s", filePath);
		exit(1);
	}
	else
	{
		for (i = 0; i < N; i++) for (j = 0; j < M; j++)
		{
			fscanf_s(file, "%f", &height_data);
			// taking the relative size (6 represents the maximum height)
			vertices2[i][j] = (height_data / 1917.81) * MAX_RELATIVE_SIZE;
		}
	}
}

void load_terrain_Texture(char *filePath)
{
	FILE *fd;
	int  k, n, m;
	char ch;
	int i, j;
	int red, green, blue;
	char input[100] = "";

	errno_t err;
	err = fopen_s(&fd, filePath, "r");
	if (err != 0)
	{
		printf("Could not open the file %s\n", filePath);
		exit(1);
	}
	else
	{
		fscanf_s(fd, "%[^\n] ", input, _countof(input));
		if (input[0] != 'P' || input[1] != '3')
		{
			printf("%s is not a PPM file\n", input);
			exit(1);
		}
		fscanf_s(fd, "%c", &ch);
		while (ch == '#')
		{
			fscanf_s(fd, "%[^\n] ", input, _countof(input));
			fscanf_s(fd, "%c", &ch);
		}
		ungetc(ch, fd);
		fscanf_s(fd, "%d %d %d", &n, &m, &k);

		printf("%d rows  %d columns  max value= %d\n", n, m, k);


		for (i = 0; i < n; i++) {
			for (j = 0; j < m; j++)
			{
				fscanf_s(fd, "%d %d %d", &red, &green, &blue);
				image[i][j][0] = red;
				image[i][j][1] = green;
				image[i][j][2] = blue;
			}
		}

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		glGenTextures(1, &textures[0]);
		glBindTexture(GL_TEXTURE_2D, textures[0]);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // set the pixel storge mode
		glEnable(GL_TEXTURE_2D);

		// Texture parameters
		glTexImage2D(GL_TEXTURE_2D, 0, 3, n, m, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	}
}

// Function that draws the polygonal mesh
void terrain_mesh()
{


	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (i = 0; i < N - 1; i++)	for (j = 1; j < M - 1; j++){
		glBegin(GL_QUADS);

		glTexCoord2f(0.0, 0.0);
		glVertex3i(i, vertices2[i][j], j);
		glTexCoord2f(0.0, 1.0);

		glVertex3i(i + 1, vertices2[i + 1][j], j);
		glTexCoord2f(1.0, 0.0);

		glVertex3i(i + 1, vertices2[i + 1][j + 1], j + 1);
		glTexCoord2f(1.0, 1.0);

		glVertex3i(i, vertices2[i][j + 1], j + 1);
		glEnd();
	}
	glFlush(); glDisable(GL_LIGHTING);
}

void display()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();
	gluLookAt(viewer[0], viewer[1], viewer[2], viewer[0] + viewer[3], viewer[1] + viewer[4], viewer[2] + viewer[5],
		0.0, 1.0, 0.0); // camera position


	glRotatef(theta[0], 1.0, 0.0, 0.0);
	glRotatef(theta[1], 0.0, 1.0, 0.0);

	terrain_mesh();
	draw_Sun();
	draw_spheres(GL_RENDER);

	// enable blending
	glEnable(GL_BLEND);

	// enable read-only depth buffer
	glDepthMask(GL_FALSE);

	// set the blend function for transparency 
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	// set back to normal (writable) depth buffer mode 
	glDepthMask(GL_TRUE);

	// disable blending
	glDisable(GL_BLEND);


	glFlush();
	glutSwapBuffers();


}

// Function that rotates the camera accordingly along the line of sight
void orient_view(float angle)
{
	viewer[3] = sin(angle);
	viewer[5] = -cos(angle);
}

// Function that moves the camera along the line of sight
void move_camera(int direction)
{
	viewer[0] = viewer[0] + direction * viewer[3] * 0.2;
	viewer[2] = viewer[2] + direction * viewer[5] * 0.2;
}

void keys(unsigned char key, int x, int y)
{

	if (key == 'j' || key == 'J') angle -= 0.02;	orient_view(angle);
	if (key == 'l' || key == 'L') angle += 0.02;	orient_view(angle);
	if (key == 'i' || key == 'I') move_camera(1);
	if (key == 'k' || key == 'K') move_camera(-1);
	if (key == 'u' || key == 'U') viewer[1] += 0.2;
	if (key == 'n' || key == 'N') viewer[1] -= 0.2;
	glutPostRedisplay();
}


// Function that sets the keys that rotate the scene
void special_keys(int key, int x, int y)
{

	if (key == GLUT_KEY_LEFT){
		axis = 1;
		theta[axis] -= 2.0;
		if (theta[axis] > 360.0)
			theta[axis] -= 360.0;
	}
	if (key == GLUT_KEY_RIGHT){
		axis = 1;
		theta[axis] += 2.0;
		if (theta[axis] > 360.0)
			theta[axis] -= 360.0;
	}
	if (key == GLUT_KEY_UP){
		axis = 0;
		theta[axis] += 2.0;
		if (theta[axis] > 360.0)
			theta[axis] -= 360.0;
	}
	if (key == GLUT_KEY_DOWN){
		axis = 0;
		theta[axis] -= 2.0;
		if (theta[axis] > 360.0)
			theta[axis] -= 360.0;
	}
	glutPostRedisplay();
}

// Function that processes hits for picking
void processHits(GLint hits, GLuint buffer[])
{
	unsigned int i, j;
	int choose = buffer[3];
	int depth = buffer[1];
	GLuint names, *ptr;

	if (hits == 0)
		printf("missed !\n\n");
	ptr = (GLuint *)buffer;
	for (i = 0; i < hits; i++)
	{
		names = *ptr;
		ptr += 3;
		for (j = 0; j < names; j++)
		{
			if (*ptr < 6) {
				sphere_index = *ptr;
				printf("You hit a sphere !!!\n");
			}
			ptr++;
		}
		printf("\n");
	}
}

void mouse(int button, int state, int x, int y)
{
	GLuint selectBuf[SIZE];
	GLint hits;
	GLint viewport[4];

	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		glGetIntegerv(GL_VIEWPORT, viewport);

		glSelectBuffer(SIZE, selectBuf);
		glRenderMode(GL_SELECT);

		glInitNames();
		glPushName(0);

		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();

		/* creating a 5x5 pixel picking region near around cursor's location */
		gluPickMatrix((GLdouble)x, (GLdouble)(viewport[3] - y),
			5.0, 5.0, viewport);
		gluPerspective(45.0, (GLfloat)(viewport[2] - viewport[0]) / (GLfloat)(viewport[3] - viewport[1]), 1, 10000.0);
		glMatrixMode(GL_MODELVIEW);
		draw_spheres(GL_SELECT);


		glMatrixMode(GL_PROJECTION);
		glPopMatrix();

		glMatrixMode(GL_MODELVIEW);
		hits = glRenderMode(GL_RENDER);
		processHits(hits, selectBuf);

		glutPostRedisplay();
	}
}

void myReshape(int w, int h)
{
	glViewport(0, 0, w, h);
	// Using a perspective view. This allows changing the viewing volume and maintaining the aspect ratio.
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0, w / h, 1, 10000.0);
	glMatrixMode(GL_MODELVIEW); //switch back to model view
}
void move()
{
	srand(20);
	r2 = ((double)rand() / ((double)(RAND_MAX)+(double)(1)));


	sun_pos += 0.0005;		// rotating sun					 
	for (i = 0; i < 10; i++){
		falling_speed[i] -= FS[i].f_speed;

	}
	if (sun_pos>360.0)
		sun_pos -= 360.0;

	for (i = 0; i < 10; i++){ // when the falling spheres reach the ground, they will go back again to their original position 
		if (falling_speed[i] <= 0)
			falling_speed[i] += 25.0;
	}
	glutPostRedisplay();
}

// Falling sphere colors menu
void smshading_menu(int value)
{
	if (value == 3){
		currentMaterials2 = &blueShinyMaterials;
	}
	if (value == 4){
		currentMaterials2 = &GreenShinyMaterials;
	}
}

// main menu
void mymenu(int value)
{
	if (speed>0){
		if (value == 1)
		{

			if (speed >= 4) // setting the maximum speed boundary
				exit(0);
			else
				speed += 0.005; // increase the falling speed
		}
		if (value == 2)
		{

			speed -= 0.005; // decrease the falling speed

		}

	}
	if (value == 3) exit(0);
}

void init_menu()
{
	int id, idA;

	// smooth shading submenu	
	idA = glutCreateMenu(smshading_menu);
	glutAddMenuEntry("shiny blue material", 3);
	glutAddMenuEntry("shiny green material", 4);


	// main menu
	id = glutCreateMenu(mymenu);
	glutAddMenuEntry("increase sphere speed +", 1);
	glutAddMenuEntry("decrease sphere speed -", 2);
	glutAddSubMenu("Smooth Shading Colors of spheres", idA);
	glutAddMenuEntry("Exit Window", 3);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void main(int argc, char **argv)
{
	printf("Interactive 3D Flyover Scene\n\n");
	printf("Author: Aziz Zitouni\n\n");

	LoadHeights("height_data.txt");
	for (i = 0; i < 10; i++) {
		falling_speed[i] = 25;
	}
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(500, 0);
	glutCreateWindow("Interactive 3D Flyover Scene");
	glutReshapeFunc(myReshape);
	glutIdleFunc(move);
	glutDisplayFunc(display);
	init_menu();
	glutMouseFunc(mouse);
	glutKeyboardFunc(keys);
	glutSpecialFunc(special_keys);
	glEnable(GL_DEPTH_TEST);

	load_terrain_Texture("earth.ppm");

	glutMainLoop();
}